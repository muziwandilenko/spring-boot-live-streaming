package com.codewithbisky.stream.service;

import com.codewithbisky.stream.dto.StreamDto;

import java.util.Map;

public interface StreamService {



    StreamDto validateStream(Map<String,String> map);
    StreamDto publishDone(Map<String,String> map);
}
