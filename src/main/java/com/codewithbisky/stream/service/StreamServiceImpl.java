package com.codewithbisky.stream.service;

import com.codewithbisky.stream.dto.StreamDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class StreamServiceImpl implements StreamService {

    @Override
    public StreamDto validateStream(Map<String,String> map) {


        log.info("REQUEST Body = {}",map);



        String key = map.getOrDefault("key", ""); // testKey is the key
        String streamId = map.getOrDefault("name", "");  // test is streamId

        // Obs Service : Custom
        // obs server:  rtmp://localhost:1935/live
        // obs stream Key: test?key=testKey


        // get key and userId from the  database and compare
        if(Objects.equals(key,"testKey")){ // unique key for each user

            return null;
        }

        throw new RuntimeException("Invalid Stream Key ");
    }

    @Override
    public StreamDto publishDone(Map<String, String> map) {

        log.info("PUBLISH DONE  = {}",map);
        String streamId = map.getOrDefault("name", "");  // test is userId

        //todo move all files with streamId*** to storage provider of choice ()

        return null;
    }
}
